# README #

This repository is for a script written in Python that exports tickets from Zendesk and imports them into JIRA or, more precisely, Service Desk. The script can be run multiple times and will update issues in JIRA with corresponding changes from Zendesk.

**PLEASE NOTE!** The way that the script sets comment privacy currently causes database problems because of the method of selecting the row ID for the new data. There currently isn't a working solution.

To run the script:

    python import.py [-s] [-t ID]

-s allows you to skip the name-checking phase of the script (see below for more details) while -t allows you to specify a single Zendesk ticket ID to be processed.

### What the script does ###

* Name checking.
    * This is an optional phase that allows the script to ensure that accounts exist in JIRA to correspond to all requestors, agents and interested parties on tickets in Zendesk. The script will automatically create accounts for non-company email addresses (see below for how to configure that). If there are any accounts missing for company email addresses, they are listed out and the script stops. This allows you to either map those email addresses onto existing accounts or create corresponding accounts.

* Ticket export and import.
    * The script gets the details from Zendesk either for the specified ticket ID or for all tickets under the configured account.
    * For each Zendesk ticket, the script checks to see if this has already been imported. This information is stored as a comment rather than using a custom field in order to avoid additional JIRA configuration.
    * If not yet imported, a new JIRA issue is created and a comment added with the Zendesk ticket ID.
    * The script then updates the various fields, including getting comments and attachments.

### Pre-requisites ###

A number of python libraries are referenced:

* requests
* urllib
* urllib2
* urllib3
* ssl
* json
* MySQLdb
* datetime
* dateutil
* getopt
* StringIO
* time
* shutil
* urlparse

### Script configuration ###

All customisation/configuration of the script is at the start of the file. The options are as follows:

##### Zendesk configuration #####

zd_auth allows you to specify at least one set of URL, username, API token. Please note that the script enforces the use of tokens rather than passwords because this avoids any problems that may arise through the use of SSO, e.g. Google. API tokens are created through the Zendesk web interface under Admin > API > Settings.

If you have multiple Zendesk accounts and share tickets between them, you will need to store credentials for each of the accounts. That allows the script to switch credentials if, for example, an attachment is stored under a different account.

zendesk_url specifies the root URL from where you want to export the tickets.

jira_project_name specifies the name of the project in JIRA where you want to import the tickets.

jira_url specifies the root URL for the JIRA instance.

jira_user specifies the account name to be used on JIRA for the import. It is necessary to add this account to the Service Desk Team role for the JIRA project.

jira_password specifies the password for jira_user.

jira_issue_type_name specifies which issue type you want these issues to be created under.

jira_sd_request_participants specifies the custom field ID for Request Participants. This varies per installation! It is *strongly* recommended that MySQL support is enabled (see below for more details) so that the script can figure out the ID for itself.

max_attachment_size specifies the maximum attachment size in JIRA. This allows the script to avoid downloading attachments from Zendesk that are too big to upload.

only_closed_tickets allows you to only export & import tickets that are closed.

##### Email configuration #####

The script treats external customers differently from internal customers. It does this because you may not want the script to start creating accounts for staff who have left, for example. company_domains allows you to specify the email addresses that are used by internal customers. Anyone matching one of those domains will not have an account created for them if one is needed.

Furthermore, it is possible that requesters have used multiple email addresses when submitting tickets. email_map allows you to map those different addresses onto a standardised address so that you don't end up with multiple accounts for the same person.

##### MySQL support #####

The script uses JIRA's REST API as much as possible in order to perform the import process. However, not all functionality required by the script is available through the REST API. In order to provide as full a fidelity import as possible, some parts of the script use direct MySQL interaction:

* Private comments. The method used by Service Desk to set comment privacy is not entirely accessible through the REST API. If MySQL interaction is not enabled, the script will *not* transfer private comments.
* Multi-lingual filenames. There is either a problem with the REST API or the requests library when it comes to handling multi-lingual filenames in attachments. If MySQL interaction is not enabled, files will still be uploaded but the filenames will be wrong if they contain characters outside the primary ASCII character set.
* Authors, Create and Update dates. There is no support in the REST API to explicitly set any of this information. If MySQL interaction is not enabled, comments will be prefixed with additional text to show who wrote the comment but everything will appear to have been done by the import account. Similarly, all activity will appear to have taken place at the time of running the script.
* Request Participants. This is a custom field used by Service Desk. If MySQL interaction is not enabled, it will be necessary to specify the correct field ID in the script.

Note that with the exception of comment privacy, all MySQL interaction is to existing data in the database. Comment privacy requires the creation of new data and ~~whilst every effort has been taken to ensure the approach is safe, it is *strongly* recommended that nobody else uses JIRA while the script is running.~~ will currently cause problems because the method used to select row IDs is incompatible with how Service Desk/JIRA works. There **isn't** currently a solution although research is urgently being carried out.